
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NotFound from "./scenes/notFoundPage/NotFound";
import Home from "./scenes/Home/home";
import Footer from "./component/footer";
import SignupPage from "./scenes/authPage/SignupPage";
import LoginPage from "./scenes/authPage/LoginPage";
import 'normalize.css';
import UserProfile from "./scenes/user/userProfile";
import RestaurantPage from "./scenes/restaurant/RestaurantPage";
import StartPage from "./scenes/startPage";
import {toast,ToastContainer} from "react-toastify";

const App = () => {
  return (
      <div>
          <ToastContainer
              autoClose={5000}
              position={toast.POSITION.TOP_RIGHT}
          />
          <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={StartPage} />
                  <Route path="/signup" exact component={SignupPage} />
                  <Route path="/login" exact component={LoginPage} />
                  <Route path="/home" exact component={Home} />
                  <Route path="/restaurant" exact component={RestaurantPage}/>
                  <Route path="/user" component={UserProfile}/>
                  <Route component={NotFound} />
              </Switch>
          </BrowserRouter>
          <Footer />
      </div>
  );
};

export default App;
