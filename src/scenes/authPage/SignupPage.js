import React, { Component } from "react";
import {toast} from "react-toastify";
import PropTypes from "prop-types";
import 'react-toastify/dist/ReactToastify.css';
import "./AuthPage.css";
import Loader from "../../component/loading";
import axios from 'axios';
import {urlsConfig} from "../../services/axios/config";
axios.defaults.baseURL = urlsConfig.baseUrl;







class SignupPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirm: "",
      phone: "",
      loading: false,
      success:true
    };
  }

  componentDidMount() {
    if(localStorage.getItem("token") && localStorage.getItem("username")){
      this.props.history.push('/user');
    }
    document.title = "صفحه ی ثبت‌نام";
  }



  signup(firstName, lastName ,email , password , phone) {
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(password);
    console.log(phone);
    this.setState(prevState => ({
      loading: !prevState.loading
    }));

    axios.post(urlsConfig.signup, {
      firstName, lastName,email, password, phone
    })
        .then(response=>{
      localStorage.setItem("token", response.data.token);
      localStorage.setItem("username", response.data.username);
      this.setState(prevState=>({
        success: !prevState.success
      }));
          this.props.history.push({
            pathname: '/user',
          });
          toast.success("ثبت نام با موفقیت انجام شد");
    })
        .catch(error => {
      this.setState(prevState=>({
        loading : !prevState.loading,
        success:  !prevState.success
      }));
      toast.error(error.response.data.message);

    });


  }





  formValidationCheck = event => {
    event.preventDefault();
    if (!this.state.firstName) {
      toast.error("فیلد نام نمی تواند خالی باشد");
      return;
    }
    if (
        !this.state.firstName.match(/^[a-zA-Z-" "]+$/iu) &&
        !this.state.firstName.match(
            /^[\u0600-\u06FF\s]+$/
        )
    ) {
      toast.error("فبلد نام وارد شده معتبر نیست");
      return;
    }
    if (!this.state.lastName) {
      toast.error("فیلد نام خانوادگی نمی تواند خالی باشد");
      return;
    }
    if (
        !this.state.lastName.match(/^[a-zA-Z-" "]+$/iu) &&
        !this.state.lastName.match(
            /^[\u0600-\u06FF\s]+$/
        )
    ) {
      toast.error("فبلد نام خانوادگی وارد شده معتبر نیست");
      return;
    }

    if (!this.state.email) {
      toast.error("ایمیل نمی‌تواند خالی باشد");
      return;
    }

    if (!this.state.password) {
      toast.error("رمز عبور نمی‌تواند خالی باشد");
      return;
    }
    if (!this.state.passwordConfirm) {
      toast.error("فبلد تایید رمز عبور نمی‌تواند خالی باشد");
      return;
    }

    if (!(this.state.password === this.state.passwordConfirm)) {
      toast.error("مقادیر رمز عبورهای وارد شده برابر نیستند");
      return;
    }

    if (!this.state.phone) {
      toast.error("شماره تلفن همراه نمی‌تواند خالی باشد");
      return;
    }
    if (!this.state.phone.match(
        /^[0-9]+$/
    )) {
      toast.error("شماره تلفن همراه معتبر نمی باشد");
      return;
    }

    this.signup(this.state.firstName,this.state.lastName,this.state.email,this.state.password, this.state.phone);




  };



  render() {
    if (this.state.loading) {
      return (
          <Loader/>
      );
    } else {
      return (
          <div className="main-login-signup">
            <ul className="slideshow">
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
            </ul>
            <div className="row justify-content-center">
              <div className="col-lg-6 col-md-7 col-sm-9 justify-content-center">
                <form id="signup_form" className="border" onSubmit={this.formValidationCheck}>
                  <div className="border-bottom" id="signup_form_title">
                    <b>ثبت نام</b>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-3 col-form-label">نام : </label>
                    <div className="col-sm-9">
                      <input
                          type="text"
                          className="form-control"
                          id="name_input"
                          placeholder="نام"
                          value={this.state.firstName}
                          onChange={event => {
                            this.setState({firstName: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-3 col-form-label">
                      نام خانوادگی :{" "}
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="text"
                          className="form-control"
                          id="lastname_input"
                          placeholder="نام خانوادگی"
                          value={this.state.lastName}
                          onChange={event => {
                            this.setState({lastName: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-3 col-form-label">
                      {" "}
                      نام کاربری/ایمیل :{" "}
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="text"
                          className="form-control"
                          id="username_input"
                          placeholder="نام کاربری/ایمیل"
                          value={this.state.email}
                          onChange={event => {
                            this.setState({email: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label
                        htmlFor="password_input"
                        className="col-sm-3 col-form-label"
                    >
                      پسورد :
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="password"
                          className="form-control"
                          id="password_input"
                          placeholder="پسورد"
                          value={this.state.password}
                          onChange={event => {
                            this.setState({password: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label
                        htmlFor="password_input_confirm"
                        className="col-sm-3 col-form-label"
                    >
                      تکرار پسورد :
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="password"
                          className="form-control"
                          id="password_input_confirm"
                          placeholder="تکرار پسورد"
                          value={this.state.passwordConfirm}
                          onChange={event => {
                            this.setState({passwordConfirm: event.target.value});
                          }}
                      />
                    </div>
                  </div>


                  <div className="form-group row">
                    <label className="col-sm-3 col-form-label">
                      {" "}
                      تلفن همراه :{" "}
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="text"
                          className="form-control"
                          placeholder="تلفن همراه"
                          id="phone_input"
                          value={this.state.phone}
                          onChange={event => {
                            this.setState({phone: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <a onClick={() => {
                    this.props.history.push("/login")
                  }}>قبلا ثبت نام کردید ؟</a>

                  <div className="text-center">
                    <button
                        type="submit"
                        className="btn submit_butten mb-2"
                        // onClick={() => {
                        //   this.props.history.push("/");
                        // }}
                    >
                      ثبت نام
                    </button>
                    <button type="button" className="btn cancel_button mb-2">
                      انصراف
                    </button>
                  </div>

                </form>
                <br/>
                <br/>
                <br/>
                <br/>
              </div>
            </div>


          </div>
      );
    }
  }
}

SignupPage.propTypes = {
  history: PropTypes.shape({
    action: PropTypes.string,
    block: PropTypes.func,
    createHref: PropTypes.func,
    go: PropTypes.func,
    goBack: PropTypes.func,
    goForward: PropTypes.func,
    length: PropTypes.number,
    listen: PropTypes.func,
    location: PropTypes.object,
    push: PropTypes.func,
    replace: PropTypes.func
  }),

  location: PropTypes.shape({
    hash: PropTypes.string,
    key: PropTypes.string,
    pathname: PropTypes.string,
    search: PropTypes.string,
    state: PropTypes.object
  }),

  match: PropTypes.shape({
    isExact: PropTypes.bool,
    params: PropTypes.shape({
      projectId: PropTypes.string
    }),
    path: PropTypes.string,
    url: PropTypes.string
  }),

  staticContext: PropTypes.object
};

export default SignupPage;
