import React, { Component } from "react";
import { toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import 'react-toastify/dist/ReactToastify.min.css';
import "./AuthPage.css";
import Loader from "../../component/loading";
import PropTypes from "prop-types";
import  GoogleLogin  from 'react-google-login';
import axios from 'axios';
import {urlsConfig} from "../../services/axios/config";
axios.defaults.baseURL = urlsConfig.baseUrl;




class LoginPage extends Component {
  constructor(props) {
    super(props);


    this.state = {
      username: "",
      password: "",
      loading: false,
      success: false
    };
  }

  componentDidMount() {
    if(localStorage.getItem("token") && localStorage.getItem("username")){
      this.props.history.push('/user');
    }
    document.title = "ورود";
  }


  login(username, password) {
    this.setState(prevState => ({
      loading: !prevState.loading
    }));

    axios.post(urlsConfig.login, {
      username, password,
    })
        .then(response => {
          console.log(response.data);
          localStorage.setItem("username", response.data.username);
          localStorage.setItem("token", response.data.token);
          this.setState(prevState => ({
            success: !prevState.success
          }));
          toast.success(" با موفقیت وارد شدید");
          this.props.history.push("/user");
        })
        .catch(error => {
          console.log(error.response.data.message);
          // handle error
            this.setState(prevState=>({
              loading : !prevState.loading,
              success:  !prevState.success
            }))
          toast.error(error.response.data.message);
          this.props.history.push("/signup");
        });
  }

  formValidationCheck = event => {
    event.preventDefault();
    if (!this.state.username) {
      toast.error("نام کاربری نمی تواند خالی باشد ");
      return;
    }


    if (!this.state.password) {
      toast.error("رمز عبور نمی‌تواند خالی باشد");
      return;
    }

    this.login(this.state.username, this.state.password);


  };



  googleLoginSuccess = (response)=>{
    console.log(response);
    let idToken = response.wc.id_token;
    console.log(idToken);
    this.setState(prevState => ({
      loading: !prevState.loading
    }));
    axios.post(urlsConfig.googleLogin, {
      idToken
    })
        .then(response => {
          console.log(response.data);
          localStorage.setItem("username", response.data.username);
          localStorage.setItem("token", response.data.token);
          this.setState(prevState => ({
            loading: !prevState.loading
          }));
          toast.success(" با موفقیت وارد شدید");
          this.props.history.push("/user");
        })
        .catch(error => {

          // handle error
          this.setState(prevState=>({
            loading : !prevState.loading,
            success:  !prevState.success
          }));
          toast.error(error.response.data.message);
          this.props.history.push("/signup");

        });
  }

  googleLoginFailure = (response) =>{
    toast.error("خطای گوگل");
    this.props.history.push("/signup");
  }







  render() {
    if (this.state.loading) {
      return (
          <Loader/>
      )
    } else {
      return (
          <div className="main-login-signup">
            <ul className="slideshow">
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
              <li>
                <span/>
              </li>
            </ul>
            <div className="row justify-content-center">
              <div className="col-lg-6 col-md-7 col-sm-9 justify-content-center">
                <form
                    id="signup_form"
                    className="border"
                    onSubmit={this.formValidationCheck}
                >
                  <div className="border-bottom" id="signup_form_title">
                    <b>ورود</b>
                  </div>

                  <br/>
                  <div className="form-group row">
                    <label className="col-sm-3 col-form-label">
                      {" "}
                      نام کاربری :{" "}
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="text"
                          className="form-control"
                          id="username_input"
                          placeholder="نام کاربری/ایمیل"
                          value={this.state.username}
                          onChange={event => {
                            this.setState({username: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label
                        htmlFor="password_input"
                        className="col-sm-3 col-form-label"
                    >
                      پسورد :
                    </label>
                    <div className="col-sm-9">
                      <input
                          type="password"
                          className="form-control"
                          id="password_input"
                          placeholder="پسورد"
                          value={this.state.password}
                          onChange={event => {
                            this.setState({password: event.target.value});
                          }}
                      />
                    </div>
                  </div>

                  <a onClick={() => {
                    this.props.history.push("/signup")
                  }}>ثبت نام نکرده اید؟</a>
                  <div className="text-center">
                    <GoogleLogin
                        clientId="917688244498-jte5pkllkdt43dc356ofu9umdsple4df.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.googleLoginSuccess}
                        onFailure={this.googleLoginFailure}
                        cookiePolicy={'single_host_origin'}
                    />
                    <button type="submit" className="btn submit_butten mb-2">
                      ورود
                    </button>
                    <button type="button" className="btn cancel_button mb-2">
                      انصراف
                    </button>
                  </div>
                </form>
                <br/>
                <br/>
                <br/>
                <br/>
              </div>
            </div>
          </div>
      );
    }
  }
}

LoginPage.propTypes = {
  history: PropTypes.shape({
    action: PropTypes.string,
    block: PropTypes.func,
    createHref: PropTypes.func,
    go: PropTypes.func,
    goBack: PropTypes.func,
    goForward: PropTypes.func,
    length: PropTypes.number,
    listen: PropTypes.func,
    location: PropTypes.object,
    push: PropTypes.func,
    replace: PropTypes.func
  }),

  location: PropTypes.shape({
    hash: PropTypes.string,
    key: PropTypes.string,
    pathname: PropTypes.string,
    search: PropTypes.string,
    state: PropTypes.object
  }),

  match: PropTypes.shape({
    isExact: PropTypes.bool,
    params: PropTypes.shape({
      projectId: PropTypes.string
    }),
    path: PropTypes.string,
    url: PropTypes.string
  }),

  staticContext: PropTypes.object
};



export default LoginPage;
