import React, { Component } from "react";


import '../../../node_modules/normalize.css/normalize.css';
import "./NotFound.css";
import PropTypes from "prop-types";



class NotFound extends Component {
  render() {
    return (
      <div id="notfound" className="notfound-body">
        <div className="notfound">
          <div className="notfound-404">
            <h1>:(</h1>
          </div>
          <h2>404 - محتوا یافت نشد</h2>
          <p>
            صفحه‌ی مد نظر شما ممکن است حذف شده باشد یا تغییر نام داده باشد یا
            آنکه به صورت موقت از دستررس خارج شده است
          </p>
          <span onClick={()=>{this.props.history.push("/")}}>بازگشت به خانه</span>
        </div>
      </div>
    );
  }
}

NotFound.propTypes = {
  history: PropTypes.shape({
    action: PropTypes.string,
    block: PropTypes.func,
    createHref: PropTypes.func,
    go: PropTypes.func,
    goBack: PropTypes.func,
    goForward: PropTypes.func,
    length: PropTypes.number,
    listen: PropTypes.func,
    location: PropTypes.object,
    push: PropTypes.func,
    replace: PropTypes.func
  }),

  location: PropTypes.shape({
    hash: PropTypes.string,
    key: PropTypes.string,
    pathname: PropTypes.string,
    search: PropTypes.string,
    state: PropTypes.object
  }),

  match: PropTypes.shape({
    isExact: PropTypes.bool,
    params: PropTypes.shape({
      projectId: PropTypes.string
    }),
    path: PropTypes.string,
    url: PropTypes.string
  }),

  staticContext: PropTypes.object
};

export default NotFound;
