import React, {Component} from "react";



class BasketModal extends Component {

    render() {
        return (
            <div className="modal" id={"ModalBasket"}>>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content myModal">
                        <div className="modal-body">
                            <div className="col-12 d-inline-block border cart">
                                <div className="col-lg-5 col-md-7 border-bottom"><h4>سبد خرید</h4></div>
                                <div className="col-10 list">
                                    {this.props.showListFoodInBasket()}
                                    <div style={{paddingTop:"20px"}} className="col-10">
                                        <h5>   جمع کل:
                                            <b style={{margin:"10px"}}>{this.props.totalMoney} تومان </b>
                                        </h5>
                                    </div>
                                    <div  className="col-10">
                                        <button type="button" className="btn confirm-btn" data-dismiss="modal"
                                                  onClick={(e)=>{
                                            this.props.finalizeOrder();
                                        }}>تایید نهایی</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



export default BasketModal;