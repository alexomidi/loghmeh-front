import React, {Component} from "react";


class BuyBasket extends Component{
    render(){
        return(
            <div className="col-12 d-inline-block border cart">
                <div className="col-lg-5 col-md-7 border-bottom"><h4>سبد خرید</h4></div>
                <div className="col-10 list">
                    {this.props.funcShowItem()}
                    <div style={{paddingTop:"20px"}} className="col-10">
                        <h5>   جمع کل:
                            <b style={{margin:"10px"}}>{this.props.totalMoney} تومان </b>
                        </h5>
                    </div>
                    <div  className="col-10">
                        <button  className="btn confirm-btn" onClick={(e)=>{
                            this.props.funcFinalize();
                        }}>تایید نهایی</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default BuyBasket;