import React, { Component } from "react";


import "react-toastify/dist/ReactToastify.css";
import "../../../user/userProfile.css";


import "./index.css";


class FoodInBasket extends Component {
    render() {
        return (
            <div className="col-12 border-bottom cart-item">
                <div className="col-5 d-inline-block text-right">{this.props.foodName}<br/> &nbsp;</div>
                <div className="col-5 d-inline-block text-center">
                    <p><i data-dismiss="modal" className="flaticon-plus right-icon-plus" style={{color: "green"}}
                    onClick={(e)=>{
                        this.props.funcInc(this.props.id, this.props.foodName , this.props.foodCount);
                    }}/>{this.props.foodCount}
                        <i data-dismiss="modal" className="flaticon-minus" style={{color: "red"}}
                        onClick={(e)=>{
                            this.props.funcDec(this.props.id, this.props.foodName , this.props.foodCount);
                        }}/></p>
                    <p style={{color: "gray", fontSize: "1.5ex"}}>
                        {Number(this.props.foodCount) * Number(this.props.foodPrice)}</p>
                </div>
            </div>
        );
    }
}

export default FoodInBasket;
