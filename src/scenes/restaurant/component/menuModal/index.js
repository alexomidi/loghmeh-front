
import React, { Component } from "react";
import "react-toastify/dist/ReactToastify.css";
import "../../../user/userProfile.css";
import { toast} from "react-toastify";

class MenuModal extends Component{
    state ={num:2}


    render() {
        return (
            <div className="modal"  id={"menuModal"+this.props.id} style={{direction: "rtl"}}>>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content myModal">

                        <div className="modal-header border-0 text-center">
                            <div className="col-12">
                                <h4 className="modal-title text-center">{this.props.resturantname}</h4>
                            </div>
                        </div>


                        <div className="modal-body row">
                            <div className="col-6 d-inline-block">
                                <img alt="" src={this.props.menuUrlImage} className="modal-image"/>
                            </div>
                            <div className="col-6 d-inline-block">
                                <div className=" text-right modal-body-content">
                                    <div className="d-inline-block">
                                        <h5><b>{this.props.menuName}</b></h5>
                                    </div>
                                    <div className="d-inline-block" style={{color: "#7c9192",paddingRight:"5px"}}>
                                        {this.props.menuPopularity}
                                        <img alt="" src="./star.png" className="d-inline-block"
                                              style={{width: "20px" , paddingRight:"5px"}}/>
                                    </div>
                                </div>

                                <div className="modal-body-content text-right" style={{color: "#7c9192", fontSize: "10px"}}>
                                    {this.props.menuDescription}
                                </div>

                                <div style={{color: "#7c9192"}} className="text-right modal-body-content">
                                    <h6>{this.props.menuPrice} تومان </h6>
                                </div>
                            </div>
                        </div>

                        <div className="modal-footer">
                            <div className="col-12">
                                <div className="d-inline-block">
                                    <i className="flaticon-plus right-icon-plus" style={{color: "green"}}
                                    onClick={()=>{
                                        this.setState({
                                            num: this.state.num + 1
                                        })
                                    }}/>{
                                    this.state.num}
                                    <i className="flaticon-minus" style={{color: "red"}}
                                    onClick={(e)=>{
                                        if(this.state.num>1) {
                                            this.setState({
                                                num: this.state.num - 1
                                            })
                                        }
                                        else{
                                            toast.error("نمی توان تعدا غذا را کمتر کرد");
                                        }
                                    }}/>
                                </div>
                                <div className="d-inline-block">
                                    <button type="button"  data-dismiss="modal"
                                            className="btn confirm-btn" onClick={(e)=>{
                                        this.props.myfunc(this.props.id,this.props.menuName,this.props.menuPrice, this.state.num);
                                    }}>افزودن به سبد</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default MenuModal;