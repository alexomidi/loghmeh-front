
import React, { Component } from "react";
import "react-toastify/dist/ReactToastify.css";
import "../../../user/userProfile.css";


import "./restaurantMenu.css";



class RestaurantMenu extends Component{
    render() {
        return (
            <div id={"menu"+this.props.id} className="col-lg-3 col-md-5 d-inline-block border menu-content">
                <img alt="" src={this.props.urlImage}/>
                <div className="col-12 text-center">
                    <div>
                        <div className="col-7 d-inline-block" style={{paddingTop:"10px"}}>
                            <h5><b>{this.props.name}</b></h5>
                        </div>
                        <div className="col-4 d-inline-block" style={{color: "#7c9192"}}>
                            {this.props.popularity}<img alt="" src="./star.png" className="d-inline-block"
                                                        style={{width: "20px" , paddingRight:"5px"}}/>
                        </div>
                    </div>
                    <div style={{color: "#7c9192"}}>
                        <h6>{this.props.price} تومان </h6>
                    </div>
                </div>
                <div className="col-12 text-center">
                    <button type="button" className="btn btn-exist" data-toggle="modal" data-target={"#menuModal" + this.props.id}>افزودن به سبد</button>
                </div>
            </div>

        );
    }
}

export default RestaurantMenu;