import React, { Component } from "react";

import '../../../node_modules/normalize.css/normalize.css';
import "react-toastify/dist/ReactToastify.css";
import "./RestaurantPage.css";
import RestaurantMenu from "./component/restaurantMenu/restaurantMenu";
import Header from "../../component/header";
import Loader from "../../component/loading";
import MenuModal from "./component/menuModal";
import {toast} from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import axios from 'axios';
import {urlsConfig} from "../../services/axios/config";
import BuyBasket from "./component/BuyBasket";
import FoodInBasket from "./component/foodInBasket";
import BasketModal from "./component/basketModal";
axios.defaults.baseURL = urlsConfig.baseUrl;



class RestaurantPage extends Component{

    constructor(props) {
        super(props);
        this.state = {restaurantInfo : { restaurantName:'' , logoUrl:'' , restaurantId:'' ,
                menus:[{menuId:'', foodName:'' , popularity:'' , price:'', urlImage:'', description:'' , restaurantId:''}]} ,
            loading:true ,
        userCurrOrder : {username:'', orderId: '',
            totalFood: '' , foods:[{menuId:'', foodName:'', countFood:'', foodPrice:''}] , totalMoney:''}};

        this.showMenu = this.showMenu.bind(this);
        this.createModal = this.createModal.bind(this);
        this.addFoodToCart = this.addFoodToCart.bind(this);
        this.showListFoodInBasket = this.showListFoodInBasket.bind(this);
        this.finalizeOrder = this.finalizeOrder.bind(this);
        this.increaseCountFoodInCart = this.increaseCountFoodInCart.bind(this);
        this.decreaseCountFoodInCart = this.decreaseCountFoodInCart.bind(this);
    }

    componentDidMount() {
        if(localStorage.getItem("username") && localStorage.getItem("token")){
            document.title = "صفحه ی رستوران";
            this.fetchData();
        }
        else{
            toast.error("ابندا لاگین کنید")
            this.props.history.push("/login");
        }
    }

    fetchData(){
        const  restaurantId  = this.props.location.state.id
        axios.all([axios.get(urlsConfig.restaurant(restaurantId),
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}}),
            axios.get(urlsConfig.show_cart,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})])
            .then(axios.spread((...responses) => {
            const responseOne = responses[0];
            const responseTwo = responses[1];
            console.log("restaurantInfo:");
            console.log(responseOne.data);
            console.log("userCurrOrder:");
            console.log(responseTwo.data);
            this.setState(prevState => ({
                restaurantInfo: responseOne.data,
                userCurrOrder: responseTwo.data,
                loading: !prevState.loading
            }));
        })).catch(errors => {
            toast.error(errors.response.data.message);
            this.props.history.push("/notFound");
        })
    }

    showMenu() {
        return this.state.restaurantInfo&&this.state.restaurantInfo.menus
            &&this.state.restaurantInfo.menus.map((menu, index) =>
            <RestaurantMenu key={"menu"+menu.menuId}
                            id={menu.menuId}
                            name={menu.foodName}
                            popularity={menu.popularity}
                            price={menu.price}
                            urlImage={menu.urlImage}/>
        );
    }

    updateBasketBuy(){
        console.log("update basket fetch api to get basket...");
        axios.get(urlsConfig.show_cart,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                console.log("response data after fetch to update basket:");
                console.log(response.data);
                this.setState({ userCurrOrder: response.data });
            })
            .catch(error => {
                toast.error(error.response.data.message);
                this.props.history.push("/notFound");
            });
    }

    addFoodToCart(menuId, foodName , price ,  foodCount, e){
        const  restaurantId  = this.props.location.state.id
        console.log("user want to add " + foodCount + " " + foodName +
            " menu id is: " + menuId + " from restaurant: " +restaurantId + "price is " + price);
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.add_cart, {
            restaurantId: restaurantId ,
            menuId: menuId,
            foodName : foodName,
            price: price,
            foodCount: foodCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                toast.success(response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
            })
            .catch(error => {
            this.setState(prevState => ({
                loading: !prevState.loading,
            }));
            toast.error(error.response.data.message);
        });
    }

    showListFoodInBasket(){
        return this.state.userCurrOrder&&this.state.userCurrOrder.foods&&
            this.state.userCurrOrder.foods.map((item,index)=>
        <FoodInBasket key={"BasketItem" + item.menuId}
                      id={item.menuId}
                      foodName={item.foodName}
                      foodPrice={item.foodPrice}
                      foodCount={item.countFood}
                      funcInc={this.increaseCountFoodInCart}
                      funcDec={this.decreaseCountFoodInCart}/>);
    }

    createModal() {
        return this.state.restaurantInfo&&this.state.restaurantInfo.menus&&
            this.state.restaurantInfo.menus.map((menu , index) =>
                <MenuModal resturantname={this.state.restaurantInfo.restaurantName}
                           menuUrlImage={menu.urlImage}
                           menuName={menu.foodName}
                           menuDescription={menu.description}
                           menuPrice={menu.price}
                           menuPopularity={menu.popularity}
                           id={menu.menuId}
                           key={"menuModal" + menu.menuId}
                           myfunc={this.addFoodToCart}
                />
            );
    }

    increaseCountFoodInCart(menuId, foodName , currCount){
        console.log("user increase count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.increase_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }






    finalizeOrder(){
        console.log("user wants to finalize order");
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        axios.get(urlsConfig.finalize_order,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                toast.error(error.response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                // this.props.history.push("/notFound");
            });
    }

    decreaseCountFoodInCart(menuId, foodName , currCount){
        console.log("user decrease count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.decrease_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }






    render() {
        if (this.state.loading) {
            return (
                <Loader/>
            );
        } else {
            return (
                <div>
                    <div style={{position: "fixed", width: "100%", top: "0", zIndex: "5"}}>
                        <Header num_food_in_cart={this.state.userCurrOrder.totalFood}/>
                    </div>

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 title-div text-center">
                                <img alt={""} src={this.state.restaurantInfo.logoUrl}
                                     className="img-thumbnail restaurant-icon"/>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 text-center restaurant-name">
                                <h2 style={{fontSize: "30px"}}>{this.state.restaurantInfo.name}</h2></div>
                        </div>
                    </div>

                    <div className="container-fluid" style={{paddingBottom: "4em"}}>
                        <div className="row content-div">
                            <div className="col-lg-4 col-md-12">
                                <div className="col-lg-2 col-md-4
                menu_title text-center border-secondary border-0 d-block"><h2>&nbsp;</h2></div>
                                <div className="col-12 row">
                                    <BuyBasket totalMoney={this.state.userCurrOrder.totalMoney}
                                               funcShowItem={this.showListFoodInBasket}
                                    funcFinalize={this.finalizeOrder}/>

                                </div>
                            </div>

                            <div className="col-lg-8 col-md-12">
                                <div
                                    className="col-lg-2 col-md-4 menu_title text-center border-secondary border-bottom d-block">
                                    <h2 style={{fontSize: "30px"}}>منوی غذا</h2></div>
                                <div id="menus" className="col-12 menu row">
                                    {this.showMenu()}
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.createModal()}
                    <BasketModal totalMoney={this.state.userCurrOrder.totalMoney}
                                 showListFoodInBasket={this.showListFoodInBasket}
                                 finalizeOrder={this.finalizeOrder}/>

                </div>
            );
        }
    }
}

export default RestaurantPage;