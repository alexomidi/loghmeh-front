import React, {Component} from "react";




class  StartPage extends Component{

    render() {
        return (
            <div>
                <nav style={{position: "fixed",width :"100%" , top: "0", zIndex:"5" }}
                     className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" style={{color:"white"}}
                       data-toggle="modal" data-target="#InfoModal">لقمه پیشگام در سفارش آنلاین غذا در ایران</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ورود
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a className="dropdown-item" href="/signup">ثبت نام</a>
                                    <a className="dropdown-item" href="/login">ورود</a>
                                    <div className="dropdown-divider"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>


        <img style={{marginTop:"-10px"}} className="bg" src="https://static.snapp-food.com/bundles/bodofoodfrontend/images/newhomepage/d3.jpg" alt="logo"/>

            </div>
        );
    }

}

export default StartPage;