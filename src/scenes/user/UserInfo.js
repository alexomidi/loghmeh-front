import React, { Component } from "react";

import "./UserInfo.css";
import 'normalize.css';

class UserInfo extends Component {
    render() {
        return (
            <div id="user-card">
                <div className="username">
                    <i className="flaticon-account"></i>
                    <p>{this.props.firstname + " " + this.props.lastname}</p>
                </div>

                <div className="phone">
                    <i className="flaticon-phone"></i>
                    <p>{this.props.phone}</p>
                </div>

                <div className="mail">
                    <i className="flaticon-mail"></i>
                    <p>{this.props.email}</p>
                </div>

                <div className="credit">
                    <i className="flaticon-card"></i>
                    <p>{this.props.credit} تومان  </p>
                </div>
            </div>
        );
    }
}

export default UserInfo
