import React, {Component} from "react";


import "react-toastify/dist/ReactToastify.css";
import "./userProfile.css";
import Header from "../../component/header"
import UserInfo from "./UserInfo";
import 'normalize.css';
import {toast} from "react-toastify";
import Loader from "../../component/loading";
import axios from 'axios';
import {urlsConfig} from "../../services/axios/config";
import BasketModal from "../restaurant/component/basketModal";
import FoodInBasket from "../restaurant/component/foodInBasket";
import OrderComponent from "./orderComponent";
import FactorModal from "./factorModal";

axios.defaults.baseURL = urlsConfig.baseUrl;


class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkTab: false, userInfo: {username: '', firstName: '', lastName: '', credit: '', phone: '', email: ''},
            userCurrOrder : {username:'', orderId: '',
                totalFood: '' , foods:[{menuId:'', foodName:'', countFood:'', foodPrice:''}] , totalMoney:''} ,
            userOrders: {username: '', orders: [{orderId:'', restaurantName:'', restaurantId:'',
                    status:'', state:'', totalDeliveryTime:'', deliverPersonId:'', totalMoney:'',
                    totalFood:'', foods:[{menuId:'', foodName:'', countFood:'', foodPrice:''}]}]},
            loading: true, factor: false, money_to_add: ''}

        this.addCredit = this.addCredit.bind(this);
        this.showListFoodInBasket = this.showListFoodInBasket.bind(this);
        this.finalizeOrder = this.finalizeOrder.bind(this);
        this.increaseCountFoodInCart = this.increaseCountFoodInCart.bind(this);
        this.decreaseCountFoodInCart = this.decreaseCountFoodInCart.bind(this);
        this.showOrders = this.showOrders.bind(this);
        this.addFactorModal = this.addFactorModal.bind(this);
        this.updateBasketBuy = this.updateBasketBuy.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem("username") && localStorage.getItem("token")) {
            document.title = " صفحه ی کاربری";
            this.fetchUserInfo();
        } else {
            this.props.history.push("/login");
        }

    }

    updateBasketBuy(){
        console.log("update basket fetch api to get basket...");
        axios.get(urlsConfig.show_cart,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                console.log("response data after fetch to update basket:");
                console.log(response.data);
                this.setState({ userCurrOrder: response.data });
            })
            .catch(error => {
                toast.error(error.response.data.message);
                this.props.history.push("/notFound");
            });
    }

    fetchUserInfo() {
        console.log(localStorage.getItem("username"));
        axios.all([axios.get(urlsConfig.user_info,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}}),
            axios.get(urlsConfig.show_cart,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})])
            .then(axios.spread((...responses) => {
                const responseOne = responses[0];
                const responseTwo = responses[1];
                console.log("user info:");
                console.log(responseOne.data);
                console.log("show cart info:");
                console.log(responseTwo.data);
                this.setState(prevState => ({
                    userInfo: responseOne.data ,
                    userCurrOrder: responseTwo.data ,
                    loading: !prevState.loading
                }));
            })).catch(errors => {
            toast.error(errors.response.data.message);
            this.props.history.push("/notFound");
        })
    }

    fetchUserOrders() {
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        axios.get(urlsConfig.user_orders,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then((response) => {
                console.log("******************** user orders ********************:");
                console.log(response.data);
                this.setState(prevState => ({
                    userOrders: response.data ,
                    loading: !prevState.loading
                }));
            }).catch(error => {
            console.log(error.response.data.message);
            this.props.history.push("/notFound");
        })
    }

    addCredit(e) {
        if (!this.state.money_to_add.match(
            /^[0-9]+$/
        )) {
            toast.error("مبلغ وارد شده معتبر نیست");
            return;
        }

        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        let money = this.state.money_to_add;
        console.log(money);
        axios.post(urlsConfig.add_credit, {
            money
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                axios.get(urlsConfig.user_info,
                    { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
                    .then(response => {
                        this.setState({ userInfo: response.data });
                    })
                    .catch(error => {
                        toast.error(error.response.data.message);
                        this.props.history.push("/notFound");
                    });
                this.setState(prevState => ({
                    money_to_add: '',
                    loading: !prevState.loading
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                console.log(error.response.data);
                toast.error("خطا در افزایش موجودی");
            });
    }

    showListFoodInBasket(){
        return this.state.userCurrOrder&&this.state.userCurrOrder.foods&&
            this.state.userCurrOrder.foods.map((item,index)=>
                <FoodInBasket key={"BasketItem" + item.menuId}
                              id={item.menuId}
                              foodName={item.foodName}
                              foodPrice={item.foodPrice}
                              foodCount={item.countFood}
                              funcInc={this.increaseCountFoodInCart}
                              funcDec={this.decreaseCountFoodInCart}/>);
    }

    increaseCountFoodInCart(menuId, foodName, currCount){
        console.log("user increase count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.increase_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }





    finalizeOrder(){
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        axios.get(urlsConfig.finalize_order,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                axios.get(urlsConfig.user_info,
                    { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
                    .then(response => {
                        this.setState(prevState => ({
                            userInfo:response.data,
                        }));
                        })
                    .catch(error => {
                    toast.error(error.response.data.message);
                    this.props.history.push("/notFound");
                })
                axios.get(urlsConfig.show_cart,
                    { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
                    .then(response => {
                        this.setState(prevState => ({
                            userCurrOrder :response.data,
                        }));
                    })
                    .catch(error => {
                        toast.error(error.response.data.message);
                        this.props.history.push("/notFound");
                    })
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                toast.error(error.response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
            });
    }


    decreaseCountFoodInCart(menuId, foodName , currCount){
        console.log("user decrease count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.decrease_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }



    addFactorModal() {
        if (this.state.factor) {
            return this.state.userOrders&&this.state.userOrders.orders&&
                this.state.userOrders.orders.map((order, index) =>
                    <FactorModal key={"order-factor" + order.orderId}
                                 id={"order" + order.orderId}
                                 detail={order}
                    />);
        }
        this.setState(prevState => ({
            factor: !prevState.factor,
        }));
    }


    showOrders() {
        console.log("show orders:")
        return this.state.userOrders&&this.state.userOrders.orders&&
            this.state.userOrders.orders.map((order, index) =>
            <OrderComponent key={"order" + order.orderId}
                            index={index}
                            id={"order" + order.orderId}
                            detail={order}/>);
    }


    render() {
        if (this.state.loading) {
            return (
                <Loader/>
            );
        } else {
            return (
                <div>
                    <div className="fixed-user-header">
                        <Header num_food_in_cart={this.state.userCurrOrder.totalFood}/>
                        <UserInfo firstname={this.state.userInfo.firstName}
                                  lastname={this.state.userInfo.lastName}
                                  phone={this.state.userInfo.phone}
                                  email={this.state.userInfo.email}
                                  credit={this.state.userInfo.credit}/>
                    </div>

                    <div className="main">
                        <div className="warpper">
                            <div className="tabs">
                                <div className={!this.state.checkTab?"tab":"tab red-button"} id="two-tab"
                                     onClick={() => {
                                         this.fetchUserOrders();
                                         this.setState(prevState => ({
                                             factor: !prevState.factor,
                                             checkTab : !prevState.checkTab
                                         }));
                                     }}>سفارش ها</div>
                                <div className={!this.state.checkTab?"tab red-button":"tab"} id="one-tab"
                                     onClick={ ()=> this.setState(prevState => ({
                                         checkTab : !prevState.checkTab
                                     }))}>افزایش اعتبار</div>
                            </div>
                            <div className="panels">
                                {!this.state.checkTab ?  <div className="panel" id="one-panel">
                                    <form>
                                        <div className="row">
                                            <div className="col-sm-8">
                                                <input className="form-control" type="text"
                                                       name="money"
                                                       id="money"
                                                       placeholder=" میزان افزایش اعتبار (تومان)"
                                                       value={this.state.money_to_add}
                                                       onChange={event => {
                                                           this.setState({money_to_add: event.target.value});
                                                       }}/>
                                            </div>
                                            <div className="col-sm-4">
                                                <a className="form-control" id="button"
                                                   onClick={this.addCredit}>افزایش</a>
                                            </div>

                                        </div>
                                    </form>
                                </div>:<div className="panel" id="two-panel">
                                    {this.showOrders()}
                                </div>}

                            </div>
                        </div>

                    </div>
                    <BasketModal totalMoney={this.state.userCurrOrder.totalMoney}
                                 showListFoodInBasket={this.showListFoodInBasket}
                                 finalizeOrder={this.finalizeOrder}/>
                    {this.addFactorModal()}

                </div>
            );
        }
    }
}


export default UserProfile;





