import React, {Component} from "react";
import "./index.css"

class FactorModal extends Component{
    constructor(props) {
        super(props);
        this.addRowTable = this.addRowTable.bind(this);


    }


    addRowTable(){
        console.log("here in factor modal");
        console.log(this.props.detail);
        console.log(this.props.detail&&this.props.detail.orders);
       return  this.props.detail&& this.props.detail.foods&&this.props.detail.foods.map((food,index)=>
               <tr key={food.menuId}>
                   <td>
                           {index+1}
                   </td>
                   <td >
                       {food.foodName}
                   </td>
                   <td >
                       {food.countFood}
                   </td>
                   <td >
                       {food.foodPrice}
                   </td>
               </tr>
        );
    }
    render() {
        return (
            <div className="modal" id={"ModalFactor" + this.props.id}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content myModal">
                        <div className="modal-body" >


                            <div style={{display:"flex" , justifyContent:"center" , marginBottom:"30px",
                            fontSize:"20px"}}>
                                <p>{this.props.detail.restaurantName}</p>
                            </div>

                            <table className="table" style={{width:"100%"}}>
                                <tbody>
                                <tr>
                                    <th  scope="col">ردیف</th>
                                    <th scope="col">نام غذا</th>
                                    <th scope="col">تعداد</th>
                                    <th scope="col">قیمت</th>
                                </tr>
                                {this.addRowTable()}
                                </tbody>
                            </table>

                            <p style={{display:"flex" , justifyContent:"center"}} >
                                جمع کل: {this.props.detail.totalMoney}تومان
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default FactorModal;