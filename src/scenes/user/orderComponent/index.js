

import React, {Component} from "react";


class OrderComponent extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-sm-1" id="row_num">{this.props.index}</div>
                <div className="col-sm-6" id="rest_name">{this.props.detail.restaurantName}</div>
                <div className="col-sm-5" id="status">
                    <a data-toggle="modal" data-target={"#ModalFactor" + this.props.id}
                       className="button"  id="button3"
                    style={{ backgroundColor : this.props.detail.status==="1"?"#FFE66D" :
                    this.props.detail.state === "3"?"#8FC689" :
                    this.props.detail.state === "2"?"#D2FFFC":"#FFE66"}}>{this.props.detail.state}</a>
                </div>
            </div>
        )
    }
}

export default OrderComponent;