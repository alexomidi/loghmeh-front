import React, { Component } from "react";
import "react-toastify/dist/ReactToastify.css";

import { toast} from "react-toastify";

class FoodPartyModal extends Component{
    state ={num:2}


    render() {
        return (
            <div className="modal" id={"ModalFoodParty" + this.props.id} style={{direction: "rtl"}}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content myModal">

                        <div className="modal-header border-0 text-center">
                            <div className="col-12">
                                <h4 className="modal-title text-center">{this.props.restaurantName}</h4>
                            </div>
                        </div>


                        <div className="modal-body row">
                            <div className="col-6 d-inline-block">
                                <img src={this.props.menuUrlImage} className="modal-image" alt=""/>
                            </div>
                            <div className="col-6 d-inline-block">
                                <div className=" text-right modal-body-content">
                                    <div style={{marginLeft:"10px"}} className="d-inline-block">
                                        <h5><b>
                                            {this.props.menuName}
                                        </b></h5>
                                    </div>
                                    <div className="d-inline-block" style={{color: "#7c9192"}}>
                                        {this.props.menuPopularity}
                                        <img src="./star.png" className="d-inline-block"
                                             style={{width: "20px" , marginRight:"10px"}} alt=""/>
                                    </div>
                                </div>
                                <div className="modal-body-content text-right" style={{color: "#7c9192", fontSize: "10px"}}>
                                    {this.props.menuDescription}
                                </div>
                                <div style={{color: "#7c9192"}} className="text-right modal-body-content">
                                    <div className="d-inline-block old-price">{this.props.menuOldPrice}</div>
                                    <div className="d-inline-block new-price">{this.props.menuNewPrice}</div>
                                </div>
                            </div>
                        </div>

                        <div className="modal-footer">
                            <div className="col-12">
                                <div className="existance d-inline-block col-3 text-center" style={{float: "right"}}>موجود:
                                    {this.props.menuCount}
                                </div>
                                <div className="d-inline-block">
                                    <i className="flaticon-plus right-icon-plus" style={{color: "green"}}
                                       onClick={()=>{
                                           this.setState({
                                               num: this.state.num + 1
                                           })
                                       }}/>
                                    {this.state.num}
                                    <i className="flaticon-minus" style={{color: "red"}}
                                       onClick={()=>{
                                           this.setState({
                                               num: this.state.num - 1
                                           })
                                       }}/>
                                </div>
                                <div className="d-inline-block">
                                    <button className="btn confirm-btn" data-dismiss="modal"
                                        onClick={(e)=>{
                                        if (this.state.num > this.props.menuCount){
                                            toast.error("تعداد غذای انتخاب شده بیش از تعداد غذای موجود است")
                                        }
                                        else{
                                            this.props.myfunc(this.props.restaurantId , this.props.id ,
                                                this.props.menuName ,this.props.menuNewPrice, this.state.num);
                                        }
                                    }}
                                    >افزودن به سبد</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FoodPartyModal;