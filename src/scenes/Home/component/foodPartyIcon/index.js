import React, {Component} from "react";
import 'react-multi-carousel/lib/styles.css';
import 'pure-react-carousel/dist/react-carousel.es.css';

class FoodPartyIcon extends Component{
    render() {
        return (
                <div className="food-party-list text-center" >
                    <div className="col-12" style={{marginBottom:"5px"}}>
                        <div className="d-inline-block div-img">
                            <img src={this.props.menuUrlImage} style={{width: "100px", height: "100px"}} alt=""/>
                        </div>
                        <div className="d-inline-block div-text text-right">
                            <h4>{this.props.menuName}</h4>
                            <h6>
                                {this.props.menuPopularity}
                                <img  src="./star.png" className="d-inline-block" style={{width: "20px" , paddingRight:"5px"}} alt=""/>
                            </h6>
                        </div>
                    </div>
                    <div className="col-12" >
                        <div className="old-price d-inline-block">
                            {this.props.menuOldPrice}
                        </div>
                        <div className="new-price d-inline-block">
                            {this.props.menuNewPrice}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="existance col-12">موجود: {this.props.menuCount}</div>
                        </div>
                        <div className="col-6">
                            <button className="buy-btn col-12" data-toggle="modal" data-target={"#ModalFoodParty" + this.props.id}>خرید</button>
                        </div>

                    </div>
                    <div className="row">
                        <div className="food-party-fooder">
                            {this.props.restaurantName}
                        </div>
                    </div>
                </div>
        );
    }
}

export default FoodPartyIcon;