import React, {Component} from "react";
import "./index.css"
import {Link} from "react-router-dom";

class RestaurantIcon extends Component{
    render() {
        return (
            <div className="col-lg-3 col-md-4" id={"restaurant" + this.props.id}>
                <div className="col-lg-8 offset-lg-2 col-md-12 d-inline-block border menu-content">
                    <div className="col-12 text-center">
                        <img src={this.props.urlImage} alt=""/>
                        <br/>
                        <h3 style={{marginBottom:"20px" , fontWeight:"bold"}}>{this.props.name}</h3>
                        <br/>
                        <h5 style={{marginBottom:"20px"}}>زمان تخمینی: {this.props.estimateTime}</h5>
                    </div>
                    <div className="col-12 text-center" >
                        <Link className="btn btn-exist" to={{
                            pathname: '/restaurant',
                            state: {
                                id: this.props.id
                            }
                        }}>نمایش منو</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default RestaurantIcon;