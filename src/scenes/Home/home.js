import React, { Component } from "react";


import "react-toastify/dist/ReactToastify.css";
import Header from "../../component/header"
import "./home.css"
import 'normalize.css';
import axios from "axios";
import {urlsConfig} from "../../services/axios/config";
import Loader from "../../component/loading";
import FoodInBasket from "../restaurant/component/foodInBasket";
import {toast} from "react-toastify";
import BasketModal from "../restaurant/component/basketModal";
import RestaurantIcon from "./component/RestaurantIcon";
import FoodPartyIcon from "./component/foodPartyIcon";
import FoodPartyModal from "./component/modalFoodParty";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'pure-react-carousel/dist/react-carousel.es.css';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
axios.defaults.baseURL = urlsConfig.baseUrl;



class Home extends Component{

    constructor(props) {
        super(props);
        this.state= {restaurants:[{logoUrl:'',estimateDelivery:'',name:'',id:''}] ,
            foodParties:[{restaurantId:'', restaurantName:'', restaurantLogo:'', menuName:'', menuId:'',
                menuDescription:'', menuPopularity:'', menuOldPrice:'', menuUrlImage:'',
                menuNewPrice:'', menuCount:''}] ,
            pageNumber:1 , restaurantSearchVal:"" , foodSearchVal:"",  loading:true ,
            seconds:'' , timeValue:'', isMoving:false , timerFoodParty:'' , secondsRemaining:'' ,
            userCurrOrder : {username:'', orderId: '',
                totalFood: '' , foods:[{menuId:'', foodName:'', countFood:'', foodPrice:''}] , totalMoney:''} };

        this.showListFoodInBasket = this.showListFoodInBasket.bind(this);
        this.finalizeOrder = this.finalizeOrder.bind(this);
        this.increaseCountFoodInCart = this.increaseCountFoodInCart.bind(this);
        this.decreaseCountFoodInCart = this.decreaseCountFoodInCart.bind(this);
        this.showListRestaurants = this.showListRestaurants.bind(this);
        this.showListFoodParty = this.showListFoodParty.bind(this);
        this.createFoodPartyModal = this.createFoodPartyModal.bind(this);
        this.addFoodPartyToCart = this.addFoodPartyToCart.bind(this);
        this.tick = this.tick.bind(this);
        this.startCountDown = this.startCountDown.bind(this);
        this.updateBasketBuy = this.updateBasketBuy.bind(this);
        this.fetchFoodPartyData = this.fetchFoodPartyData.bind(this);
        this.fetchRestaurantsData = this.fetchRestaurantsData.bind(this);
        this.getLoadMoreButton = this.getLoadMoreButton.bind(this);
        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount() {
        if(localStorage.getItem("username") && localStorage.getItem("token")){
            document.title ="خانه" ;
            this.fetchData();
            this.secondsRemaining;
        }
        else{
            toast.error("ابندا لاگین کنید");
            this.props.history.push("/login");
        }
        this.startCountDown();
    }

    tick() {
        if (this.secondsRemaining === 0) {
            this.fetchFoodPartyData();
        } else {
            let min = Math.floor(this.secondsRemaining / 60);
            let sec = this.secondsRemaining - (min * 60);
            this.setState({
                timeValue: min,
                seconds: sec,
            })
            if (sec < 10) {
                this.setState({
                    seconds: "0" + this.state.seconds,
                })
            }
            if (min < 10) {
                this.setState({
                    value: "0" + min,
                })
            }
            if (min === 0 & sec === 0) {
                clearInterval(this.intervalHandle);
            }
            this.secondsRemaining--
        }
    }

    startCountDown() {
        this.intervalHandle = setInterval(this.tick, 1000);
    }

    searchButtonOnClickHandler = event => {
        this.fetchRestaurantsData();
        this.setState({pageNumber:1});
    };

    fetchData(pageNumber=1){
        let uri = "/restaurants";
        if (this.state.restaurantSearchVal !== "" && this.state.foodSearchVal === "") {
            uri += "?searchRestaurant=" + this.state.restaurantSearchVal;
        }
        if (this.state.foodSearchVal !== "" && this.state.restaurantSearchVal === "") {
            uri += "?searchFood=" + this.state.foodSearchVal;
        }
        if (this.state.restaurantSearchVal !== "" && this.state.foodSearchVal !== "" ){
            uri += "?searchRestaurant=" + this.state.restaurantSearchVal;
            uri += "&searchFood=" + this.state.foodSearchVal;
        }
        if(this.state.restaurantSearchVal !== "" || this.state.foodSearchVal !== ""){
            if (pageNumber >= 1) {
                uri += "&page=" + pageNumber;
            }
        }
        else if (pageNumber >= 1) {
            uri += "?page=" + pageNumber;
        }
        axios.get(uri,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                console.log("restaurants");
                console.log(response.data);
                if (pageNumber === 1) {
                    this.setState(prevState => ({
                        restaurants: response.data,
                    }));
                } else {
                    this.setState(prevState => ({
                        restaurants: this.state.restaurants.concat(response.data) ,
                    }));
                }
            })
            .catch(function(error) {
                toast.error(error.response.data.message);
            });

        axios.all([axios.get(urlsConfig.show_cart,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}}),
            axios.get(urlsConfig.foodParties,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}}) ,
            axios.get(urlsConfig.food_party_start,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})])
            .then(axios.spread((...responses) => {
                const responseOne = responses[0]
                const responseTwo = responses[1]
                const responseThree = responses[2]
                console.log("user curr Order:");
                console.log(responseOne.data);
                console.log("available food parties:");
                console.log(responseTwo.data);
                this.secondsRemaining = 60*30 -  responseThree.data;
                this.setState(prevState => ({
                    foodParties:  responseTwo.data ,
                    userCurrOrder: responseOne.data,
                    loading: !prevState.loading
                }));
            })).catch(errors => {
            toast.error(errors.response.data.message);
            this.props.history.push("/notFound");
        })
    }

    fetchFoodPartyData(){
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        axios.all([
            axios.get(urlsConfig.foodParties,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}}) ,
            axios.get(urlsConfig.food_party_start,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})])
            .then(axios.spread((...responses) => {
                const responseOne = responses[0]
                const responseTwo = responses[1]
                console.log("available food parties:");
                console.log(responseOne.data);
                this.secondsRemaining = 60*30 -  responseTwo.data;
                this.setState(prevState => ({
                    foodParties:  responseOne.data ,
                    loading: !prevState.loading
                }));
            })).catch(errors => {
            toast.error(errors.response.data.message);
            this.props.history.push("/notFound");
        })
    }

    updateBasketBuy(){
            console.log("update basket fetch api to get basket...");
            axios.get(urlsConfig.show_cart,
                { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
                .then(response => {
                    console.log("response data after fetch to update basket:");
                    console.log(response.data);
                    this.setState({ userCurrOrder: response.data });
                })
                .catch(error => {
                    toast.error(error.response.data.message);
                    this.props.history.push("/notFound");
                });
        }


    fetchRestaurantsData(pageNumber = 1){
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        // if pageNumber == 1 change state entirely but else it would just append new data
        let uri = "/restaurants";
        if (this.state.restaurantSearchVal !== "" && this.state.foodSearchVal === "") {
            uri += "?searchRestaurant=" + this.state.restaurantSearchVal;
        }
        if (this.state.foodSearchVal !== "" && this.state.restaurantSearchVal === "") {
            uri += "?searchFood=" + this.state.foodSearchVal;
        }
        if (this.state.restaurantSearchVal !== "" && this.state.foodSearchVal !== "" ){
            uri += "?searchRestaurant=" + this.state.restaurantSearchVal;
            uri += "&searchFood=" + this.state.foodSearchVal;
        }
        if(this.state.restaurantSearchVal !== "" || this.state.foodSearchVal !== ""){
            if (pageNumber >= 1) {
                uri += "&page=" + pageNumber;
            }
        }
        else if (pageNumber >= 1) {
            uri += "?page=" + pageNumber;
        }
        axios.get(uri,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                console.log("restaurants");
                console.log(response.data);
                if (pageNumber === 1) {
                    this.setState(prevState => ({
                        restaurants: response.data,
                        loading: !prevState.loading
                    }));
                } else {
                    this.setState(prevState => ({
                        restaurants: this.state.restaurants.concat(response.data) ,
                        loading: !prevState.loading
                    }));
                }
                toast.success("جست و جو با موفقیت انجام شد");
            })
            .catch(function(error) {
                toast.error(error.response.data.message);
            });
    }


    getLoadMoreButton() {
        return <button className="btn show_more"  onClick={event=>{
            this.fetchRestaurantsData(this.state.pageNumber+1);
            this.setState({pageNumber:this.state.pageNumber+1});
        }}>نمایش بیشتر</button>;

    }

    searchButtonOnClickHandler = event => {
        this.fetchRestaurantsData();
        this.setState({pageNumber:1});
    };


    showListFoodInBasket(){
        return this.state.userCurrOrder&&this.state.userCurrOrder.foods&&
            this.state.userCurrOrder.foods.map((item,index)=>
                <FoodInBasket key={"BasketItem" + item.menuId}
                              id={item.menuId}
                              foodName={item.foodName}
                              foodPrice={item.foodPrice}
                              foodCount={item.countFood}
                              funcInc={this.increaseCountFoodInCart}
                              funcDec={this.decreaseCountFoodInCart}/>);
    }

    addFoodPartyToCart(restaurantId, menuId , foodName, price, foodCount , e){
        console.log(restaurantId);
        console.log(menuId);
        console.log(price);
        console.log(foodName);
        console.log(foodCount);
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.add_foodParty_cart, {
            restaurantId: restaurantId ,
            menuId: menuId,
            foodName : foodName,
            price:price,
            foodCount: foodCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                toast.success(response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
            })
            .catch(error => {
                console.log(error.response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            });
    }

    showListRestaurants(){
        let restaurant_list = "";
        if (this.state.restaurants)
            restaurant_list = this.state.restaurants.map(restaurant=> {
                return (
                    <RestaurantIcon key={"restaurant" + restaurant.id} id={restaurant.id}
                                    name={restaurant.name} urlImage={restaurant.logoUrl}
                                    estimateTime={restaurant.estimateDelivery}/>
                );
            });
        return (
            restaurant_list
        )
    }

    showListFoodParty(){
        let foodParty_list = "";
        if (this.state.foodParties)
            foodParty_list = this.state.foodParties.map(foodParty  => {
                return (
                    <FoodPartyIcon key={foodParty.menuId}
                                   id={foodParty.menuId}
                                   restaurantName={foodParty.restaurantName}
                                   restaurantId={foodParty.restaurantId}
                                   menuName={foodParty.menuName}
                                   menuDescription={foodParty.menuDescription}
                                   menuPopularity={foodParty.menuPopularity}
                                   menuOldPrice={foodParty.menuOldPrice}
                                   menuNewPrice={foodParty.menuNewPrice}
                                   menuUrlImage={foodParty.menuUrlImage}
                                   menuCount={foodParty.menuCount}
                    />
                );
            });
        return (
            foodParty_list
        )
    }

    createFoodPartyModal(){
        return this.state.foodParties&&this.state.foodParties.map((foodParty) =>
            <FoodPartyModal key={"ModalFoodParty"+foodParty.menuId}
                            id={foodParty.menuId}
                            restaurantName={foodParty.restaurantName}
                            restaurantId={foodParty.restaurantId}
                            menuName={foodParty.menuName}
                            menuDescription={foodParty.menuDescription}
                            menuPopularity={foodParty.menuPopularity}
                            menuOldPrice={foodParty.menuOldPrice}
                            menuNewPrice={foodParty.menuNewPrice}
                            menuUrlImage={foodParty.menuUrlImage}
                            menuCount={foodParty.menuCount}
                            myfunc={this.addFoodPartyToCart}
            />
        );
    }


    increaseCountFoodInCart(menuId, foodName,  currCount){
        console.log("user increase count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.increase_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }









    finalizeOrder(){
        console.log("user wants to finalize order");
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
        axios.get(urlsConfig.finalize_order,
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(responseFinal => {
                this.fetchFoodPartyData();
                this.updateBasketBuy();
                console.log(responseFinal.data.message);
                toast.success(responseFinal.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));

            })
            .catch(error => {
                toast.error(error.response.data.message);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
            });
    }
    decreaseCountFoodInCart(menuId, foodName , currCount){
        console.log("user decrease count food in food cart");
        this.setState(prevState => ({
            loading: !prevState.loading,
        }));
        axios.post(urlsConfig.decrease_food, {
            menuId : menuId,
            foodName : foodName,
            currCount: currCount
        },
            { headers: { "Authorization" : `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.updateBasketBuy();
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log(error.messageType);
                this.setState(prevState => ({
                    loading: !prevState.loading,
                }));
                toast.error(error.response.data.message);
            })
    }














    render() {
        const responsive = {
            desktop: {
                breakpoint: { max: 3000, min: 1024 },
                items: this.state.foodParties.length,
                slidesToSlide: this.state.foodParties.length<=5?1:Math.floor(this.state.foodParties.length/5) + 1
            },
            tablet: {
                breakpoint: { max: 1024, min: 464 },
                items: this.state.foodParties.length,
                slidesToSlide: this.state.foodParties.length/5
            },
            mobile: {
                breakpoint: { max: 464, min: 0 },
                items: this.state.foodParties.length,
                slidesToSlide:this.state.foodParties.length/5
            }
        };
        if(this.state.loading){
            return (
                <Loader/>
            );
        }
        else {
            return (
                <div>
                    <div style={{position: "fixed",width :"100%" , top: "0", zIndex:"5" }}>
                        <Header num_food_in_cart={this.state.userCurrOrder.totalFood}/>
                    </div>

                    <div className="container-fluid main-title-coutainer">
                        <div className="row">
                            <div className="col-12 text-center">
                                <img src="./LOGO.png" className="logo-img" alt=""/>
                            </div>
                            <div className="col-12 text-center">
                                اولین و بزرگ‌ترین وب‌سایت سفارش آنلاین غذا دانشگاه تهران
                            </div>
                            <div className="search-content col-lg-8 col-md-11">
                                <form action="./" method="post">
                                    <input style={{marginLeft:"10px",  borderRadius:"20px"}}
                                           type="text"
                                           placeholder="نام رستوران"
                                           name="restaurant_name"
                                           value={this.state.restaurantSearchVal}
                                           onChange={event => {
                                               this.setState({restaurantSearchVal:event.target.value});
                                           }}
                                    />
                                        <input style={{marginLeft:"10px" , borderRadius:"20px"}}
                                               type="text"
                                               placeholder="نام غذا"
                                               name="foodname"
                                               value={this.state.foodSearchVal}
                                               onChange={event => {
                                                   this.setState({foodSearchVal:event.target.value});
                                               }}
                                        />
                                    <button
                                        className="btn-search"
                                        onClick={this.searchButtonOnClickHandler}
                                    >جست‌وجو</button>
                                </form>
                            </div>
                        </div>
                    </div>



                    <div className="container-fluid food-party-container">
                        <div className="row text-center">
                            <div className="col-12 text-center">
                                <h2 style={{fontSize: "30px" , marginRight:"740px"}} className="col-lg-2 col-md-4 offset-lg-5 offset-md-4">جشن غذا</h2>
                            </div>

                            <div className="col-12">
                                <div className="col-lg-2 col-md-4 offset-lg-5 offset-md-4 time-shower">
                                    زمان باقی مانده : {this.state.timeValue + ":" + this.state.seconds}

                                </div>
                            </div>

                            <Carousel
                                swipeable={true}
                                draggable={true}
                                responsive={responsive}
                                ssr={true} // means to render carousel on server-side.
                                infinite={true}
                                // autoPlay={false}
                                keyBoardControl={false}
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                deviceType={"desktop"}
                                centerMode={true}
                                emulateTouch={false}
                                axis="horizontal"
                                dynamicHeight={true}
                                swipeScrollTolerance={5}
                                // gutter = {this.state.foodParties.length}
                            >
                                {this.showListFoodParty()}
                            </Carousel>;


                        </div>
                    </div>

                    <div className="container-fluid restaurant-list">
                        <div className="row text-center">
                            <div  className="col-12 text-center">
                                <h2 style={{fontSize: "30px" , marginRight:"740px"}} className="col-lg-2 col-md-4 offset-lg-5 offset-md-4">رستوران ها</h2>
                            </div>
                            {this.showListRestaurants()}
                        </div>
                        {this.getLoadMoreButton()}
                    </div>



                    <BasketModal totalMoney={this.state.userCurrOrder.totalMoney}
                                 showListFoodInBasket={this.showListFoodInBasket}
                                 finalizeOrder={this.finalizeOrder}/>
                    {this.createFoodPartyModal()}
                </div>
            );
        }
    }
}
export default Home;