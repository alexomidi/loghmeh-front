import React, { Component } from "react";

import "./index.css";
import 'normalize.css';
import {Link } from "react-router-dom";


class Header extends Component{
    render() {
        return (
            <div id="header">
                <Link to="/home" id="logo">
                    <img alt="" src="./LOGO.png"/>
                </Link>
                <div className="basket">
                    <i type="button" className="flaticon-smart-cart" data-toggle="modal" data-target={"#ModalBasket"}></i>
                    <div className="num_on_basket">{this.props.num_food_in_cart}</div>
                </div>
                <Link id="exit" to="/login" onClick={ ()=>
                    {
                        localStorage.removeItem("username");
                        localStorage.removeItem("token");
                    }
                }>خروج</Link>
            </div>
        );
    }
}

export default Header;



