import React, { Component } from "react";
import 'normalize.css';
import "./styles.css";

class Footer extends Component {
  render() {
    return (
      <footer className="text-center" id="footer">
        <span className="text-center" id="footer_body">
          &copy; تمامی حقوق  متعلق به لقمه می‌باشد.
        </span>
      </footer>
    );
  }
}

export default Footer;
