import React, { Component } from "react";
import "./index.css"


class Loader extends Component{
    render() {
        return(
            <div className="loading-page-body">
                <div className="loader"></div>
            </div>
        )
    }
}

export default Loader;