
export const urlsConfig = {
  baseUrl: 'http://185.166.105.6:31223/IEServletJSP/',
  login: '/login',
  googleLogin: '/googleLogin',
  signup: '/signup',
  restaurant : id => `/restaurants/${id}`,
  user_info: `/users/user_info/`,
  show_cart :  `/users/show_cart`,
  add_credit:  `/users/add_credit`,
  add_cart:  `/users/add_cart` ,
  food_party_start: `/restaurants/foodParty_start_time`,
  add_foodParty_cart:  `/users/add_foodParty_cart` ,
  increase_food:  `/users/increase`,
  decrease_food:  `/users/decrease` ,
  finalize_order: `/users/finalize` ,
  user_orders:  `/users/orders`,
  restaurants : '/restaurants',
  foodParties: '/food_parties',

};
